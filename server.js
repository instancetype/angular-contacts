/**
 * Created by instancetype on 11/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
'use strict';

const
  express = require('express')
, app = express()
, api = require('./api')
, users = require('./accounts')
, path = require('path')

const
  PORT = process.env.PORT || 3000

app
  .use(express.static(path.join(__dirname, 'public')))
  .use(users)
  .use('/api', api)

app
  .get('*', function(req, res) {
    if (!req.user) {
      res.redirect('/login')
    }
    else {
      res.sendFile('main.html', { root : path.join(__dirname, 'public') })
    }
  })

app
  .listen(PORT, function() {
    console.log('Listening on', PORT)
  })