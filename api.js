/**
 * Created by instancetype on 11/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true, curly : false,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
'use strict';

const
  express = require('express')
, Bourne = require('bourne')
, bodyParser = require('body-parser')

const
  db = new Bourne('data.json')
, router = express.Router()

router
  .use(bodyParser.json())

  .route('/contact')
    .get(function getContacts(req, res) {
      db.find({ userId : parseInt(req.user.id, 10) }, function findCb(err, data) {
        res.json(data)
      })
    })
    .post(function postContact(req, res) {
      let contact = req.body
      contact.userId = req.user.id

      db.insert(contact, function insertCb(err, data) {
        res.json(data)
      })
    })

router
  .param('id', function setDbQuery(req, res, next) {
    req.dbQuery = { id : parseInt(req.params.id, 10) }
    next()
  })
  .route('/contact/:id')
    .get(function getContact(req, res) {
      db.findOne(req.dbQuery, function findOneCb(err, data) {
        res.json(data)
      })
    })
    .put(function updateContact(req, res) {
      var contact = req.body
      delete contact.$promise
      delete contact.$resolved

      db.update(req.dbQuery, contact, function updateCb(err, data) {
        res.json(data[0])
      })
    })
    .delete(function deleteContact(req, res) {
      db.delete(req.dbQuery, function deleteContact() {
        res.json({})
      })
    })

module.exports = router