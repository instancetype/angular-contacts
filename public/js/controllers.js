/**
 * Created by instancetype on 11/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function STRICT(angular) {
  'use strict';

  angular.module('ContactsApp')
    .controller('ListController', function ListController($scope, $rootScope, Contact, $location, options) {
      $rootScope.PAGE = 'all'

      $scope.contacts = Contact.query()
      $scope.fields = ['firstName', 'lastName'].concat(options.displayed_fields)

      $scope.sort = function sort(field) {
        $scope.sort.field = field
        $scope.sort.order = !$scope.sort.order
      }

      $scope.show = function show(id) {
        $location.url('/contact/' + id)
      }

      $scope.sort.field = 'firstName'
      $scope.sort.order = false
    })

    .controller('NewController', function NewController($scope, $rootScope, Contact, $location) {
      $rootScope.PAGE = 'new'

      $scope.contact = new Contact({
        firstName : ['', 'text']
      , lastName  : ['', 'text']
      , email     : ['', 'email']
      , homePhone : ['', 'tel']
      , cellPhone : ['', 'tel']
      , birthday  : ['', 'date']
      , website   : ['', 'url']
      , address   : ['', 'text']
      })

      $scope.save = function save() {
        if ($scope.newContact.$invalid) {
          $scope.$broadcast('record:invalid')
        }
        else {
          $scope.contact.$save()
          $location.url('/contacts')
        }
      }
    })
    .controller('EntryController', function EntryController($scope, $rootScope, $location, Contact, $routeParams) {
      $rootScope.PAGE = 'entry'

      $scope.contact = Contact.get({ id : parseInt($routeParams.id, 10) })
      $scope.delete = function deleteContact() {
        $scope.contact.$delete()
        $location.url('/contacts')
      }
    })
    .controller('SettingsController', function SettingsController($scope, $rootScope, options, Fields) {
      $rootScope.PAGE = 'settings'

      $scope.allFields = []
      $scope.fields = options.displayed_fields

      Fields.headers().then(function getPossibleFields(data) {
        $scope.allFields = data
      })

      $scope.toggle = function toggle(field) {
        var i = options.displayed_fields.indexOf(field)

        if (i > -1) {
          options.displayed_fields.splice(i, 1)
        }
        else {
          options.displayed_fields.push(field)
        }
        Fields.set(options.displayed_fields)
      }
    })

}(angular))