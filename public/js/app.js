/**
 * Created by instancetype on 11/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function STRICT(angular) {
  'use strict';

  angular.module('ContactsApp', ['ngRoute', 'ngResource', 'ngMessages'])
    .config(function appConfig($routeProvider, $locationProvider) {
      $routeProvider
        .when('/contacts', {
          controller  : 'ListController'
        , templateUrl : 'views/list.html'
        })
        .when('/contact/new', {
          controller  : 'NewController'
        , templateUrl : 'views/new.html'
        })
        .when('/contact/:id', {
          controller  : 'EntryController'
        , templateUrl : 'views/entry.html'
        })
        .when('/settings', {
          controller  : 'SettingsController'
        , templateUrl : 'views/settings.html'
        })
        .otherwise({
          redirectTo : '/contacts'
        })

      $locationProvider.html5Mode(true)
    })
    .value('options', {})
    .run(function configureOptions(options, Fields) {
      Fields.get().success(function getOptionsData(data) {
        options.displayed_fields = data
      })
    })

}(angular))