/**
 * Created by instancetype on 11/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function STRICT(angular) {
  'use strict';

  angular.module('ContactsApp')
  .factory('Contact', function contactFactory($resource) {
    return $resource(
      '/api/contact/:id'
    , { id : '@id' }
    , { 'update' : { method : 'PUT' }}
    )
  })
  .factory('Fields', function fieldsFactory($q, $http, Contact) {
      var url = '/options/displayed_fields'
        , ignored = ['firstName', 'lastName', 'id', 'userId']
        , allFields = []
        , deferred = $q.defer()

        , contacts = Contact.query(function cb() {
            contacts.forEach(function collectFields(contact) {
              Object.keys(contact).forEach(function(key) {
                if (allFields.indexOf(key) < 0 && ignored.indexOf(key) < 0) {
                  allFields.push(key)
                }
              })
            })
            deferred.resolve(allFields)
          })

      return {
        get : function get() {
          return $http.get(url)
        }
      , set : function set(newFields) {
          return $http.post(url, { fields : newFields })
        }
      , headers : function headers() {
          return deferred.promise
        }
      }
    })

}(angular))