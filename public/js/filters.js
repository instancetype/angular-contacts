/**
 * Created by instancetype on 11/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function STRICT(angular) {
  'use strict';

  angular.module('ContactsApp')
    .filter('labelCase', function labelCase() {
      return function labelCase(input) {
        input = input.replace(/([A-Z])/g, ' $1')
        return input[0].toUpperCase() + input.slice(1)
      }
    })
    .filter('camelCase', function camelCase() {
      return function camelCase(input) {
        return input.toLowerCase().replace(/ (\w)/g, function subCap(match, letter) {
          return letter.toUpperCase()
        })
      }
    })
    .filter('keyFilter', function keyFilter() {
      return function keyFilter(obj, query) {
        var filtered = {}

        angular.forEach(obj, function(val, key) {
          if (key !== query) {
            filtered[key] = val
          }
        })
        return filtered
      }
    })

}(angular))