/**
 * Created by instancetype on 11/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function STRICT(angular) {
  'use strict';

  angular.module('ContactsApp')
    .value(
      'FieldTypes'
    , { text : ['Text', 'should be text']
      , email : ['Email', 'should be an email address']
      , number : ['Number', 'should be a number']
      , date : ['Date', 'should be a date']
      , datetime : ['Datetime', 'should be a datetime']
      , time : ['Time', 'should be a time']
      , month : ['Month', 'should be a month']
      , week : ['Week', 'should be a week']
      , url : ['URL', 'should be a URL']
      , tel : ['Phone Number', 'should be a phone number']
      , color : ['Color', 'should be a color']
      }
    )
    .directive('formField', function formField($timeout, FieldTypes) {
      return {
        restrict : 'EA'
      , templateUrl : 'views/form-field.html'
      , replace : true
      , scope : {
          record : '='
        , field : '@'
        , live : '@'
        , required : '@'
        }
      , link : function link($scope, element, attr) {
          var timeout

          $scope.$on('record:invalid', function onInvalid() {
            $scope[$scope.field].$setDirty()
          })

          $scope.types = FieldTypes

          $scope.remove = function remove(field) {
            delete $scope.record[field]
            $scope.blurUpdate()
          }

          $scope.blurUpdate = function blurUpdate() {
            if ($scope.live !== 'false') {
              $scope.record.$update(function blurUpdateCb(updatedRecord) {
                $scope.record = updatedRecord
              })
            }
          }

          $scope.update = function update() {
            $timeout.cancel(timeout)
            timeout = $timeout($scope.blurUpdate, 1000)
          }
        }
      }
    })
    .directive('newField', function newField($filter, FieldTypes) {
      return {
        restrict : 'EA'
      , templateUrl : 'views/new-field.html'
      , replace : true
      , scope : {
          record : '='
        , live : '@'
        }
      , require : '^form'
      , link : function link($scope, element, attr, form) {
          $scope.types = FieldTypes
          $scope.field = {}

          $scope.show = function show(type) {
            $scope.field.type = type
            $scope.display = true
          }

          $scope.add = function add() {
            if (form.newField.$valid) {
              $scope.record[$filter('camelCase')($scope.field.name)] = [$scope.field.value, $scope.field.type]
              $scope.remove()

              if ($scope.live !== 'false') {
                $scope.record.$update(function blurUpdateCb(updatedRecord) {
                  $scope.record = updatedRecord
                })
              }
            }
          }

          $scope.remove = function remove() {
            $scope.field = {}
            $scope.display = false
          }
        }
      }
    })

}(angular))